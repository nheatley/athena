/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

namespace Trk {
template<int DIM>
bool
KalmanParameterUpdator::calculateFilterStep_T(
  TrackParameters& TP,
  const AmgSymMatrix(5) & trkCov,
  const AmgVector(DIM) & measPar,
  const AmgSymMatrix(DIM) & measCov,
  int paramKey,
  int sign,
  FitQualityOnSurface& fQ) const
{
  // get the parameter vector
  const AmgVector(5)& trkPar = TP.parameters();
  // reduction matrix
  AmgMatrix(DIM, 5) H =
    m_reMatrices.expansionMatrix(paramKey).block<DIM, 5>(0, 0);
  // the projected parameters from the TrackParameters
  AmgVector(DIM) projTrkPar;
  if (paramKey == 3 || paramKey == 7 || paramKey == 15) {
    projTrkPar = trkPar.block<DIM, 1>(0, 0);
  } else {
    projTrkPar = H * trkPar;
  }

  // reduction matrix H, Kalman gain K, residual r, combined covariance R
  // residual after reduction
  const AmgVector(DIM) r = measPar - projTrkPar;
  // combined covariance after reduction
  const AmgSymMatrix(DIM) R =
    (sign * measCov + projection_T<DIM>(trkCov, paramKey)).inverse();
  // Kalman gain matrix
  const AmgMatrix(5, DIM) K = trkCov * H.transpose() * R;
  const AmgMatrix(5, 5) M = m_unitMatrix - K * H;
  // --- compute local filtered state
  const AmgVector(5) newPar = trkPar + K * r;
  // --- compute filtered covariance matrix
  // C = M * trkCov * M.T() +/- K * covRio * K.T()
  const AmgSymMatrix(5) newCov =
    M * trkCov * M.transpose() + sign * K * measCov * K.transpose();
  const double chiSquared =
    (sign > 0) ? r.transpose() * R * r : r.transpose() * (-R) * r;
  // create the FQSonSurface
  fQ = FitQualityOnSurface(chiSquared, DIM);
  // In place update of parameters
  TP.updateParameters(newPar, newCov);
  return true;
}

template<int DIM>
bool
Trk::KalmanParameterUpdator::makeChi2_T(
  FitQualityOnSurface& updatedFitQoS,
  const AmgVector(5) & trkPar,
  const AmgSymMatrix(5) & trkCov,
  const AmgVector(DIM) & measPar,
  const AmgSymMatrix(DIM) & covPar,
  int paramKey,
  int sign) const
{ // sign: -1 = updated, +1 = predicted parameters.

  const AmgMatrix(DIM, 5) H =
    m_reMatrices.expansionMatrix(paramKey).block<DIM, 5>(0, 0);
  const AmgVector(DIM) r = measPar - H * trkPar;
  // get the projected matrix
  AmgSymMatrix(DIM) R = sign * projection_T<DIM>(trkCov, paramKey);
  R += covPar;
  // calcualte the chi2 value
  double chiSquared = 0.0;
  if (R.determinant() != 0.0) {
    chiSquared = r.transpose() * R.inverse() * r;
  }
  updatedFitQoS = FitQualityOnSurface(chiSquared, DIM);
  return true;
}

template<int DIM>
AmgSymMatrix(DIM) Trk::KalmanParameterUpdator::projection_T(
  const AmgSymMatrix(5) & M,
  int key) const
{
  if (key == 3 || key == 7 || key == 15) { // shortcuts for the most common use
                                           // cases
    return M.block<DIM, DIM>(0, 0);
  } else {
    Eigen::Matrix<int, DIM, 1, 0, DIM, 1> iv;
    iv.setZero();
    for (int i = 0, k = 0; i < 5; ++i) {
      if (key & (1 << i))
        iv[k++] = i;
    }
    AmgSymMatrix(DIM) covSubMatrix;
    covSubMatrix.setZero();
    for (int i = 0; i < DIM; ++i) {
      for (int j = 0; j < DIM; ++j) {
        covSubMatrix(i, j) = M(iv(i), iv(j));
      }
    }
    return covSubMatrix;
  }
}

inline bool
KalmanParameterUpdator::thetaPhiWithinRange_5D(
  const AmgVector(5) & V,
  const KalmanParameterUpdator::RangeCheckDef rcd) const
{
  static const AmgVector(2) thetaMin(0.0, -M_PI);
  return (
    (std::abs(V(Trk::phi)) <= M_PI) && (V(Trk::theta) >= thetaMin(rcd)) &&
    (V(Trk::theta) <= M_PI));
}

inline bool
KalmanParameterUpdator::thetaWithinRange_5D(const AmgVector(5) & V) const
{
  return (V(Trk::theta) >= 0.0 && (V(Trk::theta) <= M_PI));
}

}
