//Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "LArRawEvent/LArDigitContainer.h"
#include "TBEvent/TBLArDigitContainer.h"
#include "LArRawEvent/LArCalibDigitContainer.h"
#include "LArElecCalib/ILArDigitOscillationCorrTool.h"
#include "GaudiKernel/IToolSvc.h"
#include <algorithm>

/*---------------------------------------------------------*/
template<class DIGITCONTAINER>
LArDigitPreProcessor<DIGITCONTAINER>::LArDigitPreProcessor (const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator),
    m_outputContainer ("FREE"),
    m_NumberOfSamples (0),
    m_firstSample (-1),
    m_pickGain (false),
    m_useOscillationCorrTool (false),
    m_corrTool (nullptr)
/*---------------------------------------------------------*/
{
  declareProperty("InputContainers",m_inputContainers);
  declareProperty("OutputContainer", m_outputContainer);
  declareProperty("NumberOfSamples",m_NumberOfSamples);
  declareProperty("FirstSample",m_firstSample);
  declareProperty("PickGain",m_pickGain);
  declareProperty("CorrectOscillations",m_useOscillationCorrTool);
  //Set limits:
  const float medGainFactor=9.8;  //LowGain*medGainFactor=medGain. Value correct??
  const float highGainFactor=9.8; //MedGain*highGainFactor=highGain Value correct??
  m_medGainLowerLimit=0x4fe;      //Comes from old LArRawChannelBuilder class
  m_medGainUpperLimit=0xe0f;      //Comes from old LArRawChannelBuilder class
  m_highGainLowerLimit=(short)(m_medGainUpperLimit*(1./highGainFactor)); 
  m_lowGainUpperLimit=(short)(m_medGainLowerLimit*medGainFactor);
}

/*---------------------------------------------------------*/
template<class DIGITCONTAINER>
LArDigitPreProcessor<DIGITCONTAINER>::~LArDigitPreProcessor()
{
}

/*---------------------------------------------------------*/
template<class DIGITCONTAINER>
StatusCode LArDigitPreProcessor<DIGITCONTAINER>::initialize()
/*---------------------------------------------------------*/
{
  MsgStream log(msgSvc(), name());
  StatusCode sc;  

  IToolSvc* toolSvc;
  IAlgTool* algtool;

  sc = service("ToolSvc", toolSvc);
  if (sc.isFailure()) {
    log << MSG::ERROR << " Tool Service not found " << endmsg; 
    return StatusCode::FAILURE;
  }
  
  if ( m_useOscillationCorrTool) {
    sc = toolSvc->retrieveTool("LArDigitOscillationCorrTool",algtool);
    if (sc.isFailure()) {
      log << MSG::ERROR << "Unable to find tool LArDigitOscillationCorrTool" << endmsg;
      return StatusCode::FAILURE; 
    }
    m_corrTool=dynamic_cast<ILArDigitOscillationCorrTool*>(algtool);
    if (!m_corrTool) {
      log << MSG::ERROR << "Unable to d-cast LArDigitOscillationCorrTool" << endmsg;
      return StatusCode::FAILURE;
    }
  }

  return StatusCode::SUCCESS;
}

/*---------------------------------------------------------*/
template<class DIGITCONTAINER>
StatusCode LArDigitPreProcessor<DIGITCONTAINER>::execute()
/*---------------------------------------------------------*/
{
  MsgStream log(msgSvc(), name());
  StatusCode sc;
  //prepare output container;
  LArDigitContainer* outputContainer=new LArDigitContainer();
  log << MSG::DEBUG << "Making LArDigitContainer with key " << m_outputContainer << endmsg;
  sc=evtStore()->record(outputContainer,m_outputContainer);
  if (sc.isFailure()) {
    log << MSG::WARNING << "Cannot record LArDigitContainer with key " << m_outputContainer
        << " to StoreGate." << endmsg;
    return StatusCode::FAILURE;
  }
  
  // Loop over list of input containers
  for (const std::string& cont : m_inputContainers) {
    //Retrieve input Container
    // const LArDigitContainer* inputContainer;
    const DIGITCONTAINER* inputContainer = nullptr;
    sc=evtStore()->retrieve(inputContainer,cont);
    if (sc.isFailure()) {
      log << MSG::WARNING << "Cannot retrieve DIGITCONTAINER with key " << cont
          << " from StoreGate." << endmsg;
      continue;
    }
    
    for (const auto* digit : *inputContainer) {
      const std::vector<short>& samples=digit->samples();
      HWIdentifier chid(digit->channelID());
      CaloGain::CaloGain gain=digit->gain();
      if (m_pickGain) {
	//ADD: Check if sample already there....
	short max=*(max_element(digit->samples().begin(),digit->samples().end()));
	if (!((gain==CaloGain::LARLOWGAIN && max < m_lowGainUpperLimit) || 
	      (gain==CaloGain::LARMEDIUMGAIN && max > m_medGainLowerLimit && max < m_medGainUpperLimit) ||
              (gain==CaloGain::LARHIGHGAIN && max > m_highGainLowerLimit)))
	  continue; //Gain does not match...
      }
      if (m_NumberOfSamples && samples.size()>m_NumberOfSamples) {
	std::vector<short> newSamples;
	if (m_firstSample>0) { //fixed subset of samples
	  if (m_firstSample+m_NumberOfSamples>samples.size()) {
	    log << MSG::ERROR << "Not enough samples! Have " << samples.size() << ", can't select samples " 
                << m_firstSample << " to " << m_firstSample+m_NumberOfSamples << "." << endmsg;
	    continue;
          }
	  std::vector<short>::const_iterator first=samples.begin()+m_firstSample;
	  newSamples.assign(first,first+m_NumberOfSamples);
	} else { //choose accoring to maximum sample
	  std::vector<short>::const_iterator max=max_element(digit->samples().begin(),digit->samples().end());
	  std::vector<short> newSamples;
	  const int  halfNSample=m_NumberOfSamples/2;
	  if (max-samples.begin()>halfNSample) //At lower bound
	    newSamples.assign(samples.begin(),samples.begin()+m_NumberOfSamples);
	  else if (samples.end()-1-max>halfNSample) //At upper bound
	    newSamples.assign(samples.end()-m_NumberOfSamples,samples.end());
	  else //in the middle
	    newSamples.assign(max-halfNSample,max-halfNSample+m_NumberOfSamples);
	}
	LArDigit* lardigit=new LArDigit(chid,gain,newSamples);
	outputContainer->push_back(lardigit);
      } else {
	LArDigit* lardigit=new LArDigit(chid,gain,samples);
	outputContainer->push_back(lardigit);
      }
    }//End loop over single container
  }// end loop over container-list

  if ( m_useOscillationCorrTool) {
    sc=m_corrTool->calculateEventPhase(*outputContainer);
    if (sc.isFailure()) 
      log << MSG::WARNING << "LArDigitOscillationCorrTool::calculateEventPhase failed." << endmsg;
    else {
      sc=m_corrTool->correctLArDigits(*outputContainer);
      if (sc.isFailure()) 
	log << MSG::WARNING << "LArDigitOscillationCorrTool::correctLArDigits failed." << endmsg;
    }
  }
  
  if (outputContainer->size()) {
    log << MSG::DEBUG << "Made LArDigitContainer with key " << m_outputContainer << " and size " << outputContainer->size() << endmsg;
    return StatusCode::SUCCESS;
  }
  else
    {log << MSG::ERROR << "Resulting Container is empty!" << endmsg;
    return StatusCode::FAILURE;
    }
}

/*---------------------------------------------------------*/
template<class DIGITCONTAINER>
StatusCode LArDigitPreProcessor<DIGITCONTAINER>::finalize()
/*---------------------------------------------------------*/
{
  return StatusCode::SUCCESS;
}
/*---------------------------------------------------------*/
