#include "MuonCondAlg/CscCondDbAlg.h"
#include "MuonCondAlg/MdtCalibDbAlg.h"
#include "MuonCondAlg/MdtCalibFormatAlgTest.h"
#include "MuonCondAlg/MdtCondDbAlg.h"
#include "MuonCondAlg/MuonAlignmentCondAlg.h"
#include "MuonCondAlg/MuonAlignmentErrorDbAlg.h"
#include "MuonCondAlg/RpcCondDbAlg.h"
#include "MuonCondAlg/TgcCondDbAlg.h"

DECLARE_COMPONENT(CscCondDbAlg)
DECLARE_COMPONENT(MdtCondDbAlg)
DECLARE_COMPONENT(RpcCondDbAlg)
DECLARE_COMPONENT(TgcCondDbAlg)
DECLARE_COMPONENT(MuonAlignmentErrorDbAlg)
DECLARE_COMPONENT(MuonAlignmentCondAlg)
DECLARE_COMPONENT(MdtCalibDbAlg)
DECLARE_COMPONENT(MdtCalibFormatAlgTest)
